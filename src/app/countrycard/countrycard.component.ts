import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-countrycard',
  templateUrl: './countrycard.component.html',
  styleUrls: ['./countrycard.component.css']
})
export class CountrycardComponent implements OnInit, OnChanges{
  @Input() countries: any = [];
  search: string = '';
  country1: any ;


  search1(event: any) {
    if (event === "")
      this.country1=this.countries;
    else{
      this.country1=this.countries.filter((event1:any)=>{
        return event1.name.toLowerCase().startsWith(event.toLowerCase());
      });
      }
  }
  constructor() {
  }
  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
        this.country1=this.countries;
  }

}
