import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class DummyserviceService{
  getdata(){
    let url="https://restcountries.eu/rest/v2/all";
    return this.http.get(url);
  }
  getdetail(alphacode: any){
    let url1='https://restcountries.eu/rest/v2/alpha/'+alphacode;
    return this.http.get(url1);
  }
  constructor(private http: HttpClient) { }

}

