import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {DummyserviceService} from "../dummyservice.service";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  countryDetail: any=[];
  constructor(private activatedRoute: ActivatedRoute, private httpClient: HttpClient, private dummyservice: DummyserviceService) {
  }

  alpha3Code: any;

  ngOnInit(): void {
    this.getCountryId()
  }

  getCountryId(): void {
    if (this.activatedRoute.snapshot.params.alpha3Code) {
      this.alpha3Code = this.activatedRoute.snapshot.params.alpha3Code;

      // console.log(this.activatedRoute);
      this.dummyservice.getdetail(this.alpha3Code).subscribe((data:any)=>{
        this.countryDetail=data;
        console.log(this.countryDetail);
      });

    }


  }


}
