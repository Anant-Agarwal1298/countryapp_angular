import { Component, OnInit } from '@angular/core';
import {DummyserviceService} from "../dummyservice.service";

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  countries: any=[];
  constructor(private dummyService:DummyserviceService) {
    this.dummyService.getdata().subscribe((data:any)=>{
      this.countries=data;
      console.log(this.countries);
    });
  }

  ngOnInit(): void {
  }

}
