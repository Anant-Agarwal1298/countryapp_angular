import { NgModule } from '@angular/core';
import {HomepageComponent} from "./homepage/homepage.component";
import {RouterModule, Routes} from "@angular/router";
import {DetailComponent} from "./detail/detail.component";


const routes: Routes = [
  {path: 'details/:alpha3Code', component: DetailComponent },
  {path: '', component : HomepageComponent}

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
